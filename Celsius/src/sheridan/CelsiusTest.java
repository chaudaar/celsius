package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class CelsiusTest {

	@Test
	public void testIsRegular() {
		assertTrue("Testingg for regular case", (4)== (Celsius.convertFromFahrenheit( 40)));
	}
	
	
	@Test
	public void testIsPalindromeNegative() {
		assertTrue("Testing for negative case", (-17)==(Celsius.convertFromFahrenheit(0)));
	}
	
	@Test
	public void testIsPalindromeBoundaryIn () {
		assertTrue("Testing for boundary in case", (-13)==(Celsius.convertFromFahrenheit(8)));
	}
	
	@Test
	public void testIsPalindromeBoundaryOut () {
		assertTrue("Testing for boundary out case", (-13)==(Celsius.convertFromFahrenheit(7)));
	}
}
